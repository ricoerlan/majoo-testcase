import 'package:majootestcase/models/user.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class DBProvider {
  DBProvider._();

  static final DBProvider db = DBProvider._();
  static Database? _database;

  Future<Database> get database async {
    if (_database != null) return _database!;
    _database = await initDB();
    return _database!;
  }

  Future<Database> initDB() async {
    print("initDB");
    String path = join(await getDatabasesPath(), "db_user.db");
    return await openDatabase(path, version: 1,
        onCreate: (Database db, int version) async {
      await db.execute(
          'CREATE TABLE User (id INTEGER PRIMARY KEY, email TEXT, password password, username TEXT)');
    });
  }

  Future<int> saveUser(User user) async {
    var db = await database;
    int res = await db.insert("User", user.toJson());
    return res;
  }

  Future<bool> checkUser(String user, String password) async {
    var db = await database;
    var res = await db.rawQuery(
        "SELECT * FROM User WHERE email = '$user' and password = '$password'");
    print('res $res');
    if (res.length > 0) {
      return true;
    } else {
      return false;
    }
  }
}
