class Preference {
  static const USER_INFO = "user-info";
}

class Api {
  static const BASE_URL = "";
  static const LOGIN = "/login";
  static const REGISTER = "/register";
}

class Font {
  static const H1 = 16;
  static const H2 = 14;
  static const H3 = 12;
}

class ScreenUtilConstants {
  static const width = 320.0;
  static const height = 640.0;
}
