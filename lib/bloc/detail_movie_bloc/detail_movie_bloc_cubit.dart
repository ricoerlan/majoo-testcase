import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/models/movie_model.dart';

part 'detail_movie_bloc_state.dart';

class DetailMovieBlocCubit extends Cubit<DetailMovieBlocState> {
  DetailMovieBlocCubit() : super(DetailMovieBlocInitialState());

  void getDetailMovie(Results results) async {
    emit(DetailMovieBlocInitialState());
    // ApiServices apiServices = ApiServices();
    // MovieModel movieResponse = await apiServices.getMovieList();
    // if (movieResponse.page == null) {
    //   emit(DetailMovieBlocErrorState("Error Unknown"));
    // } else {
    emit(DetailMovieBlocLoadedState(results));
    // }
  }
}
