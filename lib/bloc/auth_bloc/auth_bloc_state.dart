part of 'auth_bloc_cubit.dart';

abstract class AuthBlocState {
  const AuthBlocState();
}

class AuthBlocInitialState extends AuthBlocState {}

class AuthBlocLoadingState extends AuthBlocState {}

class AuthBlocLoggedInState extends AuthBlocState {
  final bool isLoggedIn;
  AuthBlocLoggedInState(this.isLoggedIn);
  @override
  List<Object> get props => [isLoggedIn];
}

class AuthBlocLoginState extends AuthBlocState {}

class AuthBlocSignUpState extends AuthBlocState {}

class AuthBlocSuccesState extends AuthBlocState {}

class AuthBlocLoadedState extends AuthBlocState {
  final data;

  AuthBlocLoadedState(this.data);

  @override
  List<Object> get props => [data];
}

class AuthBlocErrorState extends AuthBlocState {
  final error;

  AuthBlocErrorState(this.error);

  @override
  List<Object> get props => [error];
}
