import 'package:bloc/bloc.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/utils/db_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'auth_bloc_state.dart';

class AuthBlocCubit extends Cubit<AuthBlocState> {
  AuthBlocCubit() : super(AuthBlocInitialState());

  void fetchHistoryLogin() async {
    emit(AuthBlocInitialState());
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    bool? isLoggedIn = sharedPreferences.getBool("is_logged_in");
    if (isLoggedIn == null) {
      emit(AuthBlocLoginState());
    } else {
      if (isLoggedIn) {
        emit(AuthBlocLoggedInState(isLoggedIn));
      } else {
        emit(AuthBlocLoginState());
      }
    }
  }

  void loginUser(User user) async {
    final result = await DBProvider.db.checkUser(user.email!, user.password!);

    if (result) {
      SharedPreferences sharedPreferences =
          await SharedPreferences.getInstance();
      bool result = await sharedPreferences.setBool("is_logged_in", true);
      emit(AuthBlocLoggedInState(result));
    } else {
      emit(AuthBlocLoginState());
    }
  }

  void logoutUser() async {
    emit(AuthBlocLoadingState());
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    final result = await sharedPreferences.setBool("is_logged_in", false);
    if (result) {
      await Future<void>.delayed(const Duration(milliseconds: 2));
      emit(AuthBlocLoginState());
    }
  }

  void toSignUpPage() {
    emit(AuthBlocSignUpState());
  }

  void signUpUser(User user) async {
    try {
      emit(AuthBlocLoadingState());

      var result = await DBProvider.db.saveUser(user);
      if (result > 0) {
        SharedPreferences sharedPreferences =
            await SharedPreferences.getInstance();
        final result = await sharedPreferences.setBool("is_logged_in", true);
        emit(AuthBlocLoggedInState(result));
      } else {
        throw Exception();
      }
    } catch (e) {
      emit(AuthBlocErrorState(e));
    }
  }
}
