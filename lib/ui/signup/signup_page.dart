import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/common/widget/custom_button.dart';
import 'package:majootestcase/common/widget/text_form_field.dart';
import 'package:majootestcase/main.dart';
import 'package:majootestcase/models/user.dart';

class SignUpPage extends StatefulWidget {
  @override
  _LogoutState createState() => _LogoutState();
}

class _LogoutState extends State<SignUpPage> {
  final _usernameController = TextController(initialValue: '');
  final _emailController = TextController(initialValue: '');
  final _passwordController = TextController(initialValue: '');

  GlobalKey<FormState> formKey = new GlobalKey<FormState>();

  bool _isObscurePassword = true;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocListener<AuthBlocCubit, AuthBlocState>(
        listener: (context, state) {
          if (state is AuthBlocLoggedInState) {
            ScaffoldMessenger.of(context)
                .showSnackBar(SnackBar(content: Text('Pendaftaran Berhasil')));
          }
        },
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.only(top: 50.h, left: 25, bottom: 0, right: 25),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Image.network(
                    'https://img.freepik.com/free-vector/sign-up-concept-illustration_114360-7865.jpg'),
                SizedBox(
                  height: 9,
                ),
                _form(),
                SizedBox(
                  height: 30.h,
                ),
                CustomButton(
                  text: 'Register',
                  onPressed: handleSignUp,
                  height: 100.r,
                ),
                SizedBox(
                  height: 10.h,
                ),
                _login(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _form() {
    return Form(
      key: formKey,
      child: Column(
        children: [
          CustomTextFormField(
            context: context,
            controller: _usernameController,
            hint: 'example',
            label: 'Username',
            // validator: (String? val) {
            //   return val;
            // },
          ),
          SizedBox(height: 5.h),
          CustomTextFormField(
            context: context,
            controller: _emailController,
            isEmail: true,
            hint: 'Example@123.com',
            label: 'Email',
            validator: (val) {
              final pattern = new RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');
              String? result = '';
              if (val != null)
                result = pattern.hasMatch(val) ? null : 'email is invalid';
              return result;
            },
          ),
          SizedBox(height: 5.h),
          CustomTextFormField(
            context: context,
            label: 'Password',
            hint: 'password',
            controller: _passwordController,
            isObscureText: _isObscurePassword,
            suffixIcon: IconButton(
              icon: Icon(
                _isObscurePassword
                    ? Icons.visibility_off_outlined
                    : Icons.visibility_outlined,
              ),
              onPressed: () {
                setState(() {
                  _isObscurePassword = !_isObscurePassword;
                });
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget _login() {
    return Align(
      alignment: Alignment.center,
      child: TextButton(
        onPressed: () {
          Navigator.pop(context);
        },
        child: RichText(
          text: TextSpan(
              text: 'Sudah punya akun? ',
              style: TextStyle(color: Colors.black45),
              children: [
                TextSpan(
                  text: 'Masuk',
                ),
              ]),
        ),
      ),
    );
  }

  void handleSignUp() async {
    final _username = _usernameController.value;
    final _email = _emailController.value;
    final _password = _passwordController.value;
    if (formKey.currentState?.validate() == true &&
        _email.isNotEmpty &&
        _password.isNotEmpty) {
      User user = User(
        userName: _username,
        email: _email,
        password: _password,
      );
      BlocProvider.of<AuthBlocCubit>(context).signUpUser(user);
    }
  }
}
