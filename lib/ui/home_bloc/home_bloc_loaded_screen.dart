import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/models/movie_model.dart';
import 'package:majootestcase/ui/home_bloc/widgets/sliderListItem.dart';

import 'widgets/recommendedListItem.dart';

class HomeBlocLoadedScreen extends StatelessWidget {
  final List<Results>? data;

  HomeBlocLoadedScreen({Key? key, this.data}) : super(key: key);

  final ScrollController scrollController = ScrollController();
  final ScrollController horizontalscrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    print('data ${data?.length}');
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blueGrey.shade800,
        title: Text('Movie List'),
        actions: [
          IconButton(
              onPressed: () {
                BlocProvider.of<AuthBlocCubit>(context).logoutUser();
              },
              icon: Icon(Icons.logout_outlined))
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              height: 380,
              child: ListView.builder(
                scrollDirection: Axis.horizontal,
                controller: horizontalscrollController,
                shrinkWrap: true,
                itemCount: data?.length,
                itemBuilder: (context, index) {
                  final dataReversed = data!.reversed.toList();
                  return SliderListItem(dataReversed[index]);
                },
              ),
            ),
            SizedBox(
              height: 0,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 6),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Text(
                    'Recommended',
                    style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 5),
            ListView.builder(
              controller: scrollController,
              shrinkWrap: true,
              itemCount: data?.length,
              itemBuilder: (context, index) {
                return RecommendedListItem(data![index]);
              },
            ),
            SizedBox(
              height: 30,
            ),
          ],
        ),
      ),
    );
  }

  Widget movieItemWidget(Results? data) {
    return Card(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(12.0))),
      child: Row(
        children: [
          Padding(
            padding: EdgeInsets.symmetric(vertical: 10, horizontal: 0),
            child: Image.network(
              "https://image.tmdb.org/t/p/w500/" + data!.posterPath!,
              width: 200,
              height: 300,
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: 8, horizontal: 10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(data.name ?? "Moon Knight",
                    textDirection: TextDirection.ltr),
              ],
            ),
          )
        ],
      ),
    );
  }
}
