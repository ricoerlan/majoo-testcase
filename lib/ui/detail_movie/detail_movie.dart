import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/detail_movie_bloc/detail_movie_bloc_cubit.dart';
import 'package:majootestcase/ui/extra/loading.dart';

class MovieDetailsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        // backgroundColor: Color(0xfff4f4f4),
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          // iconTheme: IconThemeData(color: Theme.of(context).primaryColor),
        ),
        extendBodyBehindAppBar: true,
        body: BlocBuilder<DetailMovieBlocCubit, DetailMovieBlocState>(
            builder: (context, state) {
          if (state is DetailMovieBlocLoadedState) {
            return SingleChildScrollView(
              // padding: const EdgeInsets.symmetric(
              //   vertical: 10,
              //   horizontal: 20,
              // ),
              child: Column(
                children: <Widget>[
                  Center(
                    child: Hero(
                      tag: 'detail',
                      child: Container(
                        height: 450,
                        width: double.infinity,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          image: DecorationImage(
                            fit: BoxFit.cover,
                            image: NetworkImage(
                              'https://image.tmdb.org/t/p/w500/' +
                                  state.data!.backdropPath!,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    state.data!.title!,
                    style: TextStyle(
                      fontSize: 28,
                      fontWeight: FontWeight.bold,
                      letterSpacing: 2.5,
                    ),
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      //     Card(
                      //       elevation: 5,
                      //       child: Padding(
                      //         padding: const EdgeInsets.symmetric(
                      //           vertical: 10,
                      //           horizontal: 20,
                      //         ),
                      //         child: Column(
                      //           children: <Widget>[
                      //             Icon(
                      //               Icons.timer,
                      //               size: 45,
                      //               color: Theme.of(context).primaryColor,
                      //             ),
                      //             SizedBox(
                      //               height: 10,
                      //             ),
                      //             Text(
                      //               state.data!.releaseDate!,
                      //               style: TextStyle(
                      //                   fontSize: 14, fontWeight: FontWeight.bold),
                      //             ),
                      //           ],
                      //         ),
                      //       ),
                      //     ),
                      //     Card(
                      //       elevation: 5,
                      //       child: Padding(
                      //         padding: const EdgeInsets.symmetric(
                      //           vertical: 10,
                      //           horizontal: 20,
                      //         ),
                      // child:
                      Column(
                        children: <Widget>[
                          Icon(
                            Icons.calendar_today,
                            size: 35,
                            color: Theme.of(context).primaryColor,
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Text(
                            'Release Date \n${state.data!.releaseDate!}',
                            style: TextStyle(
                                fontSize: 11, fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                      //       ),
                      //     ),
                      Column(
                        children: <Widget>[
                          Icon(
                            Icons.star,
                            size: 40,
                            color: Colors.yellow,
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Text(
                            'Rating \n${state.data!.voteAverage}/10',
                            style: TextStyle(
                                fontSize: 12, fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    state.data!.overview!,
                    style: TextStyle(
                      fontSize: 18,
                      height: 1.5,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ],
              ),
            );
          }
          // },
          return LoadingIndicator();
        }));
  }
}
