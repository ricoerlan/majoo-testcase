import 'package:majootestcase/models/movie_model.dart';

import 'package:majootestcase/services/dio_config_service.dart' as dioConfig;

class ApiServices {
  Future<MovieModel> getMovieList() async {
    try {
      var dio = await dioConfig.dio();
      final response = await dio
          .get('3/trending/all/day?api_key=0bc9e6490f0a9aa230bd01e268411e10');
      final result = MovieModel.fromJson(response.data);
      return result;
    } catch (e) {
      print(e.toString());
      return MovieModel();
    }
  }
}
